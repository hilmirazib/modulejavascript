// new gridjs.Grid({
//     columns: ['Name', 'Email', 'Phone', 'Adress'],
//     data: [
//         ['Hilmi Razib Yusuf', 'hilmiyusuf107@gmail.com', '+6289657933987', 'Indonesia'],
//         ['Hilmi Smith', 'hilmismith@gmail.com', '+657875667', 'England'],
//         ['Hilmi Smith', 'hilmismith@gmail.com', '+657875667', 'England'],
//         ['Hilmi Smith', 'hilmismith@gmail.com', '+657875667', 'England'],
//         ['Hilmi Smith', 'hilmismith@gmail.com', '+657875667', 'England'],
//         ['Hilmi Smith', 'hilmismith@gmail.com', '+657875667', 'England'],
//         ['Hilmi Smith', 'hilmismith@gmail.com', '+657875667', 'England'],
//         ['Hilmi Smith', 'hilmismith@gmail.com', '+657875667', 'England'],
//         ['Hilmi Smith', 'hilmismith@gmail.com', '+657875667', 'England'],
//     ],
//     // pagination: true,
//     pagination: {
//         limit: 3,
//     },
//     sort: true,
//     fixedHeader: true,
//     search: {
//         enabled: true,
//     },
//     style: {
//         td: {
//             border: '1px solid #ccc',
//         },
//         table: {
//             'font-size': '15px',
//             padding: '50px',
//         },
//     },
//     language: {
//         search: {
//             placeholder: '🔍 Search...',
//         },
//         pagination: {
//             previous: '⬅️',
//             next: '➡️',
//             showing: '😃 Displaying',
//             results: () => 'Records',
//         },
//     },
//     className: {
//         td: 'my-td-class p-3',
//         table: 'table table-light table-striped table-hover',
//     },
// }).render(document.getElementById('wrapper'));

import Table from './tableModule.js';

const table = new Table({
    columns: ['Name', 'Email', 'Phone', 'Address'],
    data: [
        ['Hilmi Razib Yusuf', 'hilmiyusuf197@gmail.com', '08978663783', 'Salawu, Tasikmalaya'],
        ['Hilmi Yusuf', 'email.hilmiyusuf@gmail.com', '089657933987', 'Cibiru, Bandung'],
        ['Hilmi Smith', 'email.hilmismith@gmail.com', '089657934632', 'Cilenyi, Bandung'],
        ['Bonang Panji Nur', 'bpanjinur@gmail.com', '08987676544', 'Taraju, Tasikmalaya'],
        ['Hilmi Razib Yusuf', 'hilmiyusuf197@gmail.com', '08978663783', 'Salawu, Tasikmalaya'],
        ['Fiqih Hermawan', 'fiqihhermawan@gmail.com', '089657933987', 'Cibiru, Bandung'],
        ['Hilmi Yusuf', 'email.hilmiyusuf@gmail.com', '089657933987', 'Cibiru, Bandung'],
    ],
});

const wrap = document.getElementById('wrapper');
table.render(wrap);